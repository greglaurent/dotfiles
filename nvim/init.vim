call plug#begin('~/.config/nvim/plugged')

Plug 'junegunn/vim-easy-align'
Plug 'junegunn/fzf', { 'dir': '~/.fzf', 'do': './install --all' }
Plug 'neoclide/coc.nvim', { 'branch': 'release' }
Plug 'iamcco/markdown-preview.nvim', { 'do': 'cd app && yarn install'  }
Plug 'morhetz/gruvbox'
Plug 'vim-airline/vim-airline'
Plug 'vim-airline/vim-airline-themes'

call plug#end()

:set number relativenumber
:set nu rnu

" Workaround for creating transparent bg
" let g:gruvbox_transparent_bg = 1
autocmd SourcePost * highlight Normal     ctermbg=NONE guibg=NONE
    \ |    highlight LineNr     ctermbg=NONE guibg=NONE
    \ |    highlight SignColumn ctermbg=NONE guibg=NONE

colorscheme gruvbox

let g:airline_theme = 'luna'
let g:airline_powerline_fonts = 1

" set to 1, nvim will open the preview window after entering the markdown buffer
let g:mkdp_auto_start = 1

" set to 1, the nvim will auto close current preview window when change
let g:mkdp_auto_close = 1

" set to 1, the vim will refresh markdown when save the buffer or
let g:mkdp_refresh_slow = 0

" set to 1, the MarkdownPreview command can be use for all files,
let g:mkdp_command_for_global = 0

" set to 1, preview server available to others in your network
let g:mkdp_open_to_the_world = 0

" use custom IP to open preview page
let g:mkdp_open_ip = ''

" specify browser to open preview page
let g:mkdp_browser = 'firefox'

" set to 1, echo preview page url in command line when open preview page
let g:mkdp_echo_preview_url = 0

" a custom vim function name to open preview page
let g:mkdp_browserfunc = ''

" options for markdown render
" mkit: markdown-it options for render
" katex: katex options for math
" uml: markdown-it-plantuml options
" maid: mermaid options
" disable_sync_scroll: if disable sync scroll, default 0
" sync_scroll_type: 'middle', 'top' or 'relative', default value is 'middle'
"   middle: mean the cursor position alway show at the middle of the preview page
"   top: mean the vim top viewport alway show at the top of the preview page
"   relative: mean the cursor position alway show at the relative positon of the preview page
" hide_yaml_meta: if hide yaml metadata, default is 1
" sequence_diagrams: js-sequence-diagrams options
" content_editable: if enable content editable for preview page, default: v:false
" disable_filename: if disable filename header for preview page, default: 0
let g:mkdp_preview_options = {
    \ 'mkit': {},
    \ 'katex': {},
    \ 'uml': {},
    \ 'maid': {},
    \ 'disable_sync_scroll': 0,
    \ 'sync_scroll_type': 'middle',
    \ 'hide_yaml_meta': 1,
    \ 'sequence_diagrams': {},
    \ 'flowchart_diagrams': {},
    \ 'content_editable': v:false,
    \ 'disable_filename': 0
    \ }

" use a custom markdown style must be absolute path
let g:mkdp_markdown_css = ''

" use a custom highlight style must absolute path
let g:mkdp_highlight_css = ''

" use a custom port to start server or random for empty
let g:mkdp_port = ''

" preview page title
let g:mkdp_page_title = '「${name}」'

" recognized filetypes
let g:mkdp_filetypes = ['markdown', 'md']
